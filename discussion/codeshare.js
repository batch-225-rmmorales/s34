// This is used to get the contents of the express package to be used by our application
// It also allows us to access to methods and functions that will allow us to easily create a server.

const express = require("express");

// Create an application using express
// This creates an express application and stores this in a constant called app and In layman's term, app is our server.

const app = express();

// Setup for allowing the server to handle data from request
// Allows your app to read json data
// Methods used from express JS are middlewares

// For our application server to run, we need a port to listen to

const port = 3000;

app.use(express.json());

// Allows your app to read data from forms
// By the default, information recieve from the url can only be received as string or an array.
// By applying the option of "extended:true" this allows us to recieve information in other data types such as an object which we will use throughout our application.

/*
	urlencoded - is a built-in middleware function in express.js that parses the request body when the Content-type in application. 

*/

app.use(express.urlencoded({ extended: true }));

// [SECTION] Routes
// Express has methods corresponding to each HTTP method
// This route expects to retrieve a GET request at the Base URI "/"
// this route will return a simple message back to the client

app.get("/hello", (req, res) => {
  // res.send uses the express JS module's method to send a response back to the client.
  res.send("Hello from the /hello endpoint!");
});

// This routes expects to receive a POST request at the URI "/hello"

app.post("/hello", (req, res) => {
  // req.body contains the contents/data of the request body
  // All the properties defined in our Postman request will be accessible here as properties with the same names.
  res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

// Tells our server to listen to the port
// if the port is accessed, we cam run the server
// Returns a message to confirm that the server is running in the terminal.
app.listen(port, () => console.log(`Server running at port ${port}`));
