const express = require("express");

// Create an application using express
// This creates an express application and stores this in a constant called app and In layman's term, app is our server.

const app = express();

// Setup for allowing the server to handle data from request
// Allows your app to read json data
// Methods used from express JS are middlewares

// For our application server to run, we need a port to listen to

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.get("/home", (req, res) => {
  res.send("this is a simple message");
});

let users = [];
app.get("/users", (req, res) => {
  res.json(users);
});

app.post("/signup", (req, res) => {
  let alreadySignedup = false;
  if (req.body.username !== "" && req.body.password !== "") {
    users.forEach((user) => {
      if (req.body.username == user.username) {
        alreadySignedup = true;
      }
    });
    if (alreadySignedup) {
      res.send(req.body.username + " is already signed up");
    } else {
      users.push({ username: req.body.username, password: req.body.password });
      res.send("you are signed up");
    }
  } else {
    res.send("please input bothh user name and password");
  }
});

app.delete("/delete", (req, res) => {
  res.send("last user deleted");
  users.pop();
});

app.listen(port, () => console.log(`Server running at port ${port}`));
